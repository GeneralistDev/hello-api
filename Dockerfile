FROM golang:latest
ADD . app
RUN ls
RUN pwd
RUN go get github.com/revel/revel
RUN go get github.com/revel/cmd/revel
CMD ["revel", "run", "-a", "app", "-p", "9000"]